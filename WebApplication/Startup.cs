﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Microsoft.EntityFrameworkCore;
using WebApplication.Models;

namespace WebApplication
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = @"Server = tcp:alertifyserver.database.windows.net,1433; Database = AlertifyData; User ID =Alertify; Password =Pr0C0nS0l; Encrypt = true; Connection Timeout = 30;";  //Server=alertifyserver.database.windows.net;Database=AlertifyData;Trusted_Connection=True;
            services.AddDbContext<MonitorContext>(opt => opt.UseSqlServer(connection));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMvc();
        }
    }
}
