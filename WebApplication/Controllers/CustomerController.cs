﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly MonitorContext _context;

        public CustomerController(MonitorContext context)
        {
            _context = context;

            if (_context.Customers.Count() == 0)
            {
                _context.Customers.Add(new Customer { Id = 0, Address_Billing = "TBD", Contact_Name = "TBD", Email_Accounts = "TBD", Email_Alerts = "TBD", Phone_Business = "TBD", Phone_Mobile = "TBD" });
                _context.SaveChanges();
            }
        }

        // GET: api/customer
        [HttpGet]
        public IEnumerable<Customer> GetAll()       // return a list of all the customer(s) records
        {
            return _context.Customers.ToList();
        }

        // GET api/customer/5
        [HttpGet("{id}", Name = "GetCustomer")]     // GET INDIVIDUAL CUSTOMER RECORD
        public IActionResult GetById(long id)
        {
            try
            {
                var item = _context.Customers.FirstOrDefault(t => t.Id == id);
                if (item == null)
                {
                    return NotFound(ErrorCode.CustomerRecordNotFound.ToString());
                }
                return new ObjectResult(item);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateCustomer.ToString());
            }
        }



        // GET api/customer/monitors/5
        [HttpGet("monitors/{id}", Name = "GetMonitors")]     // GET MONITORS WITH CUSTOMER ID
        public IActionResult GetMonitorsByCustId(long id)
        {
            try
            {
                var monitor = _context.Monitors.FirstOrDefault(t => t.Customer_Id == id);   // Get the first monitor to see if one exists
                if (monitor == null)
                {
                    return NotFound(ErrorCode.CustomerRecordNotFound.ToString());
                }

                
                var monitors = _context.Monitors.ToList();    // construct a List the same shape
                monitors.Clear();               // then clear it of all entries
                var monitorsList = _context.Monitors.ToList();

                foreach (var item in monitorsList)
                {
                    if (item.Customer_Id == id)  // need to get all MONITORS with same Customer_Id. ie all MONITORS used by that CUSTOMER
                    {
                        monitors.Add(item);
                    }

                }

                return new ObjectResult(monitors);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateCustomer.ToString());
            }
        }



        // POST api/customer
        [HttpPost]
        public IActionResult Create([FromBody] Customer customer)
        {
            try
            {
                if (customer == null || !ModelState.IsValid)
                {
                    return BadRequest(ErrorCode.CustomerIdRequired.ToString());
                }

                bool itemExists = _context.Customers.Any(item => item.Id == customer.Id);
                if (itemExists)
                {
                    return StatusCode(StatusCodes.Status409Conflict, ErrorCode.CustomerIDInUse.ToString());
                }

                _context.Customers.Add(customer);
                _context.SaveChanges();

            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateCustomer.ToString());
            }
            
            return CreatedAtRoute("GetCustomer", new { id = customer.Id }, customer);
        }

        // PUT api/customer/5
        [HttpPut("{id}")]                               // UPDATE AN EXISTING CUSTOMER RECORD VIA (customer) ID
        public IActionResult Update(long id, [FromBody] Customer cust)
        {
            try
            {
                if (cust == null || cust.Id != id)
                {
                    return BadRequest(ErrorCode.CouldNotCreateCustomer.ToString());
                }

                var customer = _context.Customers.FirstOrDefault(t => t.Id == id);
                if (customer == null)
                {
                    return NotFound(ErrorCode.CustomerRecordNotFound.ToString());
                }

                if (cust.Customer_Name != null)
                {
                    customer.Customer_Name = cust.Customer_Name;
                }

                if (cust.Contact_Name != null)
                {
                    customer.Contact_Name = cust.Contact_Name;
                }
                
                if (cust.Address_Billing != null)
                {
                    customer.Address_Billing = cust.Address_Billing;
                }
                
                if (cust.Email_Accounts != null)
                {
                    customer.Email_Accounts = cust.Email_Accounts;
                }
                
                if (cust.Email_Alerts != null)
                {
                    customer.Email_Alerts = cust.Email_Alerts;
                }

                if (cust.Phone_Business != null)
                {
                    customer.Phone_Business = cust.Phone_Business;
                }
                
                if (cust.Phone_Mobile != null)
                {
                    customer.Phone_Mobile = cust.Phone_Mobile;
                }

                _context.Customers.Update(customer);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotUpdateCustomer.ToString());
            }
            return NoContent();
        }

        // DELETE api/customer/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                var customer = _context.Customers.FirstOrDefault(t => t.Id == id);
                if (customer == null)
                {
                    return NotFound(ErrorCode.CustomerRecordNotFound.ToString());
                }

                _context.Customers.Remove(customer);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteCustomer.ToString());
            }
            return NoContent();
        }

        public enum ErrorCode
        {
            CustomerIdRequired,
            CustomerIDInUse,
            CustomerRecordNotFound,
            CouldNotCreateCustomer,
            CouldNotUpdateCustomer,
            CouldNotDeleteCustomer
        }
    }
}
