﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    public class MonitorController : Controller
    {
        private readonly MonitorContext _context;

        public MonitorController(MonitorContext context)
        {
            _context = context;

            if (_context.Monitors.Count() == 0)
            {
                _context.Monitors.Add(new Monitor { Id = 0, Name = "Monitor", Customer_Id = 0, Location = "TBA", Limit_High = "TBD", Ip_Address = "TBR", Battery_Changed = DateTime.Now, Install_Date = DateTime.Now, Signal_Bars = 0, Signal_dBi = "TBR", Wifi_Name = "TBR" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Monitor> GetAll()
        {
            return _context.Monitors.ToList();
        }

        [HttpGet("{id}", Name = "GetMonitor")]
        public IActionResult GetById(long id)
        {
            try
            {
                var item = _context.Monitors.FirstOrDefault(t => t.Id == id);
                if (item == null)
                {
                    return NotFound(ErrorCode.RecordNotFound.ToString());
                }
                return new ObjectResult(item);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateMonitor.ToString());
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] Monitor monitor)
        {
            try
            {
                if (monitor == null || !ModelState.IsValid)
                {
                    return BadRequest(ErrorCode.MonitorNameAndCustomerAndLocationRequired.ToString());
                }

                bool itemExists = _context.Monitors.Any(item => item.Id == monitor.Id);
                if (itemExists)
                {
                    return StatusCode(StatusCodes.Status409Conflict, ErrorCode.MonitorIDInUse.ToString());
                }

                _context.Monitors.Add(monitor);
                _context.SaveChanges();

            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateMonitor.ToString());
            }
            // return Ok(monitor);
            return CreatedAtRoute("GetMonitor", new { id = monitor.Id }, monitor);
        }

        [HttpPut("{id}")]       //THIS IS THE PUT UPDATE REQUEST THAT THE MONITORS ARE SENDING
        public IActionResult Update(long id, [FromBody] Monitor item)
        {
            try
            {
                if (item == null || item.Id != id)
                {
                    return BadRequest(ErrorCode.CouldNotCreateMonitor.ToString());
                }

                var monitor = _context.Monitors.FirstOrDefault(t => t.Id == id);
                if (monitor == null)
                {
                    return NotFound(ErrorCode.RecordNotFound.ToString());
                }

                if (item.Customer_Id > 0)   // Customer_Id
                {
                    monitor.Customer_Id = item.Customer_Id;
                }
                
                if (item.Location != null)   // Location
                {
                    monitor.Location = item.Location;
                }
                
                if (item.Name != null)   // Name
                {
                    monitor.Name = item.Name;
                }
                
                if (item.Limit_High != null)   // Limit_High
                {
                    monitor.Limit_High = item.Limit_High;
                }

                if (item.Ip_Address != null)   // Ip_Address
                {
                    monitor.Ip_Address = item.Ip_Address;
                }

                if (item.Battery_Changed != null)   // Battery_Changed
                {
                    monitor.Battery_Changed = item.Battery_Changed;
                }

                if (item.Install_Date != null)   // Install_Date
                {
                    monitor.Install_Date = item.Install_Date;
                }

                if (item.Wifi_Name != null)   // Wifi_Name
                {
                    monitor.Wifi_Name = item.Wifi_Name;
                }

                if (item.Signal_dBi != null)   // Signal_dBi
                {
                    monitor.Signal_dBi = item.Signal_dBi;
                }

                if (item.Signal_Bars > 0)   // Signal_Bars
                {
                    monitor.Signal_Bars = item.Signal_Bars;
                }

                _context.Monitors.Update(monitor);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotUpdateMonitor.ToString());
            }
            return NoContent();
            //return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                var monitor = _context.Monitors.FirstOrDefault(t => t.Id == id);
                if (monitor == null)
                {
                    return NotFound(ErrorCode.RecordNotFound.ToString());
                }

                _context.Monitors.Remove(monitor);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteMonitor.ToString());
            }
            return NoContent();
        }

        public enum ErrorCode
        {
            MonitorNameAndCustomerAndLocationRequired,
            MonitorIDInUse,
            RecordNotFound,
            CouldNotCreateMonitor,
            CouldNotUpdateMonitor,
            CouldNotDeleteMonitor
        }
    }


}
