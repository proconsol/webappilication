﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    public class LogController : Controller
    {
        private readonly MonitorContext _context;

        public LogController(MonitorContext context)
        {
            _context = context;

            if (_context.Logs.Count() == 0)
            {
                _context.Logs.Add(new Log { Monitor_Id = 0, Reading = "TBR", Created_At = DateTime.Now });
                _context.SaveChanges();
            }
        }

        // GET: api/log
        [HttpGet]
        public IEnumerable<Log> GetAll()
        {
            return _context.Logs.ToList();
        }

        // GET api/log/5
        [HttpGet("{id}", Name = "GetLog")]
        public IActionResult GetById(long id)   // GET ALL LOGS WITH THE SAME MONITOR_ID
        {                                       // NOT GET INDIVIDUAL LOG
            try
            {
                var log = _context.Logs.FirstOrDefault(t => t.Monitor_Id == id);    // Check to see if any LOGS exist by that monitor
                if (log == null)
                {
                    return NotFound(ErrorCode.RecordNotFound.ToString());
                }

                var tempLogs = _context.Logs.ToList();
                tempLogs.Clear();
                var logs = _context.Logs.ToList();
                foreach (var item in logs)
                {
                    if (item.Monitor_Id == id)  // need to get all Logs with same Monitor_Id. ie all Logs made by that Monitor
                    {
                        tempLogs.Add(item);
                    }

                }
                
                return new ObjectResult(tempLogs);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateLog.ToString());
            }
        }
        
        // POST api/log
        [HttpPost]
        public IActionResult Create([FromBody] Log log)
        {
            try
            {
                if (log == null) // || !ModelState.IsValid)
                {
                    return BadRequest(ErrorCode.MonitorNameAndCustomerAndLocationRequired.ToString());
                }

                log.Created_At = DateTime.Now;
                _context.Logs.Add(log);
                _context.SaveChanges();

            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateLog.ToString());
            }
            return CreatedAtRoute("GetLog", new { id = log.Id }, log);
        }

        // PUT api/log/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/log/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                var log = _context.Logs.FirstOrDefault(t => t.Id == id);
                if (log == null)
                {
                    return NotFound(ErrorCode.RecordNotFound.ToString());
                }

                _context.Logs.Remove(log);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteLog.ToString());
            }
            return NoContent();
        }

        public enum ErrorCode
        {
            MonitorNameAndCustomerAndLocationRequired,
            MonitorIDInUse,
            RecordNotFound,
            CouldNotCreateLog,
            CouldNotUpdateLog,
            CouldNotDeleteLog
        }
    }
}
