﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Monitor
    {
        [Required]
        public long Id { get; set; }                    // unique monitor id

        public string Name { get; set; }                // descriptive name of monitor ie: Coolroom 1

        public string Location { get; set; }            // physical location of monitor ie: Ashgrove

        public long Customer_Id { get; set; }           // unique customer_id, not unique to monitor tho

        public string Limit_High { get; set; }          // alarm limit

        public string Ip_Address { get; set; }          //

        public string Wifi_Name { get; set; }           //

        public string Signal_dBi { get; set; }          // signal strength in dBm

        public int Signal_Bars { get; set; }            // signal strength represented as bars

        public DateTime Battery_Changed { get; set; }   // timestamp of last battery change

        public DateTime Install_Date { get; set; }      // timestamp of installation date
    }
}
