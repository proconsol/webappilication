﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Customer
    {
        [Required]
        public long Id { get; set; }       // unique customer_id

        public string Customer_Name { get; set; }    // 

        public string Contact_Name { get; set; }    // 

        public string Email_Accounts { get; set; }  // 

        public string Email_Alerts { get; set; }    // 

        public string Phone_Mobile { get; set; }    // 

        public string Phone_Business { get; set; }  // 

        public string Address_Billing { get; set; }  // 

    }
}
