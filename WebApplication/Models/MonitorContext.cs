﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace WebApplication.Models
{
    public class MonitorContext : DbContext
    {
        public MonitorContext(DbContextOptions<MonitorContext> options)
            : base(options)
        {
        }

        public DbSet<Monitor> Monitors { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}
