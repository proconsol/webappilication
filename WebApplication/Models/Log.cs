﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Log
    {
        
        public long Id { get; set; }

        public string Reading { get; set; }

        public DateTime Created_At { get; set; }

        public long Monitor_Id { get; set; }

        public bool Alarm_State { get; set; }
    }
}
